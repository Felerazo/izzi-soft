package com.izzisoft.proyectodh.model;


import javax.persistence.Entity;
import java.util.Date;

@Entity
public class Cargo extends  ModelBase{
    private String nombre;
    private String descripcion;
    private Date fechaInicio;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
}
