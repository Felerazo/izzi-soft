
package com.izzisoft.proyectodh.model;

import javax.persistence.Entity;

@Entity
public class PerfilEmpleado extends ModelBase {

    private int experiencia;
    private String formacion;
    private String especializacion;
    private String sector;

    public int getExperiencia() {
        return experiencia;
    }

    public String getFormacion() {
        return formacion;
    }

    public String getEspecializacion() {
        return especializacion;
    }

    public String getSector() {
        return sector;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public void setFormacion(String formacion) {
        this.formacion = formacion;
    }

    public void setEspecializacion(String especializacion)
    {
        this.especializacion = especializacion;
    }

    public void setSector(String sector)
    {
        this.sector = sector;
    }
}