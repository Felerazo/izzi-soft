package com.izzisoft.proyectodh.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Area extends ModelBase {
    private String code;
    private String nombre;

    @OneToOne(optional = false)
    private Employee responsable;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Employee getResponsable() {
        return responsable;
    }

    public void setResponsable(Employee responsable) {
        this.responsable = responsable;
    }
}
