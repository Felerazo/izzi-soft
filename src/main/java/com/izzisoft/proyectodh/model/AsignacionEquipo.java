package com.izzisoft.proyectodh.model;

import javax.persistence.Entity;
import java.util.Date;
@Entity
public class AsignacionEquipo extends ModelBase {
    private Date fecha_asignacion;
    private String encargado;
    private Date fecha_devolucion;
    private String observacion_inicial;
    private String observacion_final;

    public Date getFecha_asignacion() {
        return fecha_asignacion;
    }

    public String getEncargado() {
        return encargado;
    }

    public Date getFecha_devolucion() {
        return fecha_devolucion;
    }

    public String getObservacion_inicial() {
        return observacion_inicial;
    }

    public String getObservacion_final() {
        return observacion_final;
    }

    public void setFecha_asignacion(Date fecha_asignacion) {
        this.fecha_asignacion = fecha_asignacion;
    }

    public void setEncargado(String encargado) {
        this.encargado = encargado;
    }

    public void setFecha_devolucion(Date fecha_devolucion) {
        this.fecha_devolucion = fecha_devolucion;
    }

    public void setObservacion_inicial(String observacion_inicial) {
        this.observacion_inicial = observacion_inicial;
    }

    public void setObservacion_final(String observacio_final) {
        this.observacion_final = observacio_final;
    }
}