
package com.izzisoft.proyectodh.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class Contract extends ModelBase {

    //    employee tipo Employee, position tipo Position, initDate, endDate  tipo Date.
    @OneToOne(optional = false)
    private Employee employee;

    @OneToOne(optional = false)
    private Position position;

    @OneToOne(optional = false)
    private TipoContract tipoContract;

    @OneToOne(optional = true)
    private Suspension suspension;

    private Date initDate;
    private Date endDate;
    private String code;
    private String horario;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public TipoContract getTipoContract() {
        return tipoContract;
    }

    public void setTipoContract(TipoContract tipoContract) {
        this.tipoContract = tipoContract;
    }

    public Suspension getSuspension() {
        return suspension;
    }

    public void setSuspension(Suspension suspension) {
        this.suspension = suspension;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }
}
