package com.izzisoft.proyectodh.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class Etapa extends ModelBase {
    private String code;
    private String nombre;
    private Date fecha_inicio;
    private Date fecha_fin;
    private int nro_empleados;
    @OneToOne(optional = false)
    private Employee encargado;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public int getNro_empleados() {
        return nro_empleados;
    }

    public void setNro_empleados(int nro_empleados) {
        this.nro_empleados = nro_empleados;
    }

    public Employee getEncargado() {
        return encargado;
    }

    public void setEncargado(Employee encargado) {
        this.encargado = encargado;
    }
}
