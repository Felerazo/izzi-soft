package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.AreaCommand;
import com.izzisoft.proyectodh.model.Area;
import com.izzisoft.proyectodh.services.AreaService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/areas")
@Produces("application/json")
@CrossOrigin
public class AreaController {

    private AreaService service;

    public AreaController(AreaService service) {
        this.service = service;
    }

    @GET
    public Response getAreas(){
        List<AreaCommand> areas= new ArrayList<>();

        service.findAll().forEach(area -> {
            AreaCommand areaCommand = new AreaCommand(area);
            areas.add(areaCommand);
        });
        return Response.ok(areas).build();
    }
    @GET
    @Path("/{id}")
    public Response getItemsById(@PathParam("id") @NotNull Long id) {
        Area Area = service.findById(id);
        return Response.ok(new AreaCommand(Area)).build();
    }

    @POST
    public Response saveArea(AreaCommand Area) {
        Area model = Area.toArea();
        Area AreaPersisted = service.save(model);
        return Response.ok(new AreaCommand(AreaPersisted)).build();
    }

    @PUT
    public Response updateArea(AreaCommand area) {
        Area AreaPersisted = service.save(area.toArea());
        return Response.ok(new AreaCommand(AreaPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteArea(@PathParam("id") String id) {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }


    }

