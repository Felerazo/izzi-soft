/**
 * @author:
 */

package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.KvalueCommand;
import com.izzisoft.proyectodh.command.PositionCommand;
import com.izzisoft.proyectodh.model.Position;
import com.izzisoft.proyectodh.services.PositionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/positions")
@Produces("application/json")
@CrossOrigin
public class PositionController {
    private PositionService service;

    public PositionController(PositionService service) {
        this.service = service;
    }

    @GET
    public Response getPositions(@Context UriInfo uriInfo) {
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        if(queryParams.isEmpty()){
            List<PositionCommand> positions = new ArrayList<>();
            service.findAll().forEach(position -> {
                PositionCommand positionCommand = new PositionCommand(position);
                positions.add(positionCommand);
            });
            return Response.ok(positions).build();
        } else {
            if(queryParams.containsKey("kv")){
                List<KvalueCommand> vals = new ArrayList<>();
                 service.findAll().forEach(position -> {
                    KvalueCommand kvCommand = new KvalueCommand(position.getId().toString(),position.getCode()+"-"+position.getName());
                    vals.add(kvCommand);
                });
                    return Response.ok(vals).build();
            } else {
                return Response.ok().build();
            }
        }
    }

    @GET
    @Path("/{id}")
    public Response getItemsById(@PathParam("id") @NotNull Long id) {
        Position Position = service.findById(id);
        return Response.ok(new PositionCommand(Position)).build();
    }

    @POST
    public Response savePosition(PositionCommand Position) {
        Position model = Position.toPosition();
        Position PositionPersisted = service.save(model);
        return Response.ok(new PositionCommand(PositionPersisted)).build();
    }

    @PUT
    public Response updateItem(PositionCommand Position) {
        Position PositionPersisted = service.save(Position.toPosition());
        return Response.ok(new PositionCommand(PositionPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteItem(@PathParam("id") String id) {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

}