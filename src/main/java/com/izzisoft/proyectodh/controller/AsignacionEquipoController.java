/**
 * @author:
 */

package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.AsignacionEquipoCommand;
import com.izzisoft.proyectodh.model.AsignacionEquipo;
import com.izzisoft.proyectodh.services.AsignacionEquipoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/asignacionEquipos")
@Produces("application/json")
@CrossOrigin
public class AsignacionEquipoController {
    private AsignacionEquipoService service;

    public AsignacionEquipoController(AsignacionEquipoService service) {
        this.service = service;
    }

    @GET
    public Response getAsignacionEquipos() {
        List<AsignacionEquipoCommand> asignacionEquipos = new ArrayList<>();
        service.findAll().forEach(asignacionEquipo -> {
            AsignacionEquipoCommand asignacionEquipoCommand = new AsignacionEquipoCommand(asignacionEquipo);
            asignacionEquipos.add(asignacionEquipoCommand);
        });
        return Response.ok(asignacionEquipos).build();
    }

    @GET
    @Path("/{id}")
    public Response getAsignacionEquiposById(@PathParam("id") @NotNull Long id) {
        AsignacionEquipo AsignacionEquipo = service.findById(id);
        return Response.ok(new AsignacionEquipoCommand(AsignacionEquipo)).build();
    }

    @POST
    public Response saveAsignacionEquipo(AsignacionEquipoCommand AsignacionEquipo) {
        AsignacionEquipo model = AsignacionEquipo.toAsignacionEquipo();
        AsignacionEquipo AsignacionEquipoPersisted = service.save(model);
        return Response.ok(new AsignacionEquipoCommand(AsignacionEquipoPersisted)).build();
    }

    @PUT
    public Response updateAsignacionEquipo(AsignacionEquipoCommand AsignacionEquipo) {
        AsignacionEquipo AsignacionEquipoPersisted = service.save(AsignacionEquipo.toAsignacionEquipo());
        return Response.ok(new AsignacionEquipoCommand(AsignacionEquipoPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteAsignacionEquipo(@PathParam("id") String id) {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

}