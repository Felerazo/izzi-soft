package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Obra;
import com.izzisoft.proyectodh.repositories.ObraRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ObraServiceImpl extends GenericServiceImpl<Obra> implements ObraService {
    private static Logger logger = LoggerFactory.getLogger(ObraServiceImpl.class);

    private ObraRepository repository;

    public ObraServiceImpl(ObraRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Obra, Long> getRepository() {
        return repository;
    }
}
