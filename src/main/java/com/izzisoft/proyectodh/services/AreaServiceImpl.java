package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Area;
import com.izzisoft.proyectodh.repositories.AreaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class AreaServiceImpl extends GenericServiceImpl<Area> implements AreaService {
    private AreaRepository repository;

    public AreaServiceImpl(AreaRepository repository) {
        this.repository = repository;
    }


    @Override
    protected CrudRepository<Area, Long> getRepository() {
        return repository;
    }

    @Override
    public List<Area> findByNombre(String nombre) {
        return StringUtils.isEmpty(nombre) ? findAll() : repository.findByNombre(nombre).get();
    }
}