
package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Position;
import com.izzisoft.proyectodh.repositories.PositionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PositionServiceImpl extends GenericServiceImpl<Position> implements PositionService {
    private static Logger logger = LoggerFactory.getLogger(PositionServiceImpl.class);

    private PositionRepository repository;

    public PositionServiceImpl(PositionRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Position, Long> getRepository() {
        return repository;
    }

}