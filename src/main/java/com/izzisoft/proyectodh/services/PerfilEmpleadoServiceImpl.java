package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.PerfilEmpleado;
import com.izzisoft.proyectodh.repositories.PerfilEmpleadoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class PerfilEmpleadoServiceImpl extends GenericServiceImpl<PerfilEmpleado> implements PerfilEmpleadoService {
    private static Logger logger = LoggerFactory.getLogger(PerfilEmpleadoServiceImpl.class);

    private PerfilEmpleadoRepository repository;

    public PerfilEmpleadoServiceImpl(PerfilEmpleadoRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<PerfilEmpleado, Long> getRepository() {
        return repository;
    }
}