package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Obra;

public interface ObraService extends GenericService<Obra> {
}
