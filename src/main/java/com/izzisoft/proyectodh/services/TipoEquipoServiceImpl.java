package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.TipoEquipo;
import com.izzisoft.proyectodh.repositories.TipoEquipoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoEquipoServiceImpl extends GenericServiceImpl<TipoEquipo> implements TipoEquipoService {
    private static Logger logger = LoggerFactory.getLogger(TipoEquipoServiceImpl.class);

    private TipoEquipoRepository repository;

    public TipoEquipoServiceImpl(TipoEquipoRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<TipoEquipo, Long> getRepository() {
        return repository;
    }
}