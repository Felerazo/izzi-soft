/**
 * @author: edson
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Suspension;
import com.izzisoft.proyectodh.repositories.SuspensionRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class SuspensionServiceImpl extends GenericServiceImpl<Suspension> implements SuspensionService {
    private SuspensionRepository repository;

    public SuspensionServiceImpl(SuspensionRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Suspension, Long> getRepository() {
        return repository;
    }
}