/**
 * @author:
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.AsignacionEquipo;

public interface AsignacionEquipoService extends GenericService<AsignacionEquipo> {
}