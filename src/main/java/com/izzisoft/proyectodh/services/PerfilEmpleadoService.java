/**
 * @author:
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.PerfilEmpleado;

public interface PerfilEmpleadoService extends GenericService<PerfilEmpleado> {
}