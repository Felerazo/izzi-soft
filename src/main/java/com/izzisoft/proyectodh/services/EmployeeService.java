/**
 * @author: Edson A. Terceros T.
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Employee;

import java.io.InputStream;

public interface EmployeeService extends GenericService<Employee> {
    void saveImage(Long id, InputStream inputStream);
}