package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.Etapa;
import com.izzisoft.proyectodh.model.ModelBase;

import java.util.Date;

public class EtapaCommand extends ModelBase {
    private String code;
    private String nombre;
    private Date fecha_inicio;
    private Date fecha_fin;
    private int nro_empleados;
    //Entidad Encargado
    private String encargado;
    private Long id;

    public EtapaCommand(Etapa etapa) {
        setId(etapa.getId());
        setVersion(etapa.getVersion());
        setCreatedOn(etapa.getCreatedOn());
        setUpdatedOn(etapa.getUpdatedOn());
        this.setCode(etapa.getCode());
        this.setNombre(etapa.getNombre());
        this.setFecha_inicio(etapa.getFecha_inicio());
        this.setFecha_fin(etapa.getFecha_fin());
        this.setNro_empleados(etapa.getNro_empleados());
        //this.setEncargado(etapa.getEncargado());

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public int getNro_empleados() {
        return nro_empleados;
    }

    public void setNro_empleados(int nro_empleados) {
        this.nro_empleados = nro_empleados;
    }

    public String getEncargado() {
        return encargado;
    }

    public void setEncargado(String encargado) {
        this.encargado = encargado;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Etapa toEtapa() {
        Etapa etapa = new Etapa();
        etapa.setId(getId());
        etapa.setCode(this.getCode());
        etapa.setNombre(this.getNombre());
        etapa.setFecha_inicio(this.getFecha_inicio());
        etapa.setFecha_fin(this.getFecha_fin());
        etapa.setNro_empleados(this.getNro_empleados());
        //etapa.setEncargado(this.getEncargado());
        return etapa;
    }
}
