/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.ModelBase;
import com.izzisoft.proyectodh.model.PerfilEmpleado;

public class PerfilEmpleadoCommand extends ModelBase {

    private int experiencia;
    private String formacion;
    private String especializacion;
    private String sector;
    private Long id;

    public PerfilEmpleadoCommand(PerfilEmpleado perfilEmpleado) {
        setId(perfilEmpleado.getId());
        setVersion(perfilEmpleado.getVersion());
        setCreatedOn(perfilEmpleado.getCreatedOn());
        setUpdatedOn(perfilEmpleado.getUpdatedOn());
        this.setExperiencia(perfilEmpleado.getExperiencia());
        this.setFormacion(perfilEmpleado.getFormacion());
        this.setEspecializacion(perfilEmpleado.getEspecializacion());
        this.setSector(perfilEmpleado.getSector());
    }

    public PerfilEmpleadoCommand() {

    }

    public int getExperiencia() {
        return experiencia;
    }

    public String getFormacion() {
        return formacion;
    }

    public String getEspecializacion() {
        return especializacion;
    }

    public String getSector() {
        return sector;
    }

    

    public Long getId() {
        return id;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public void setFormacion(String formacion) {
        this.formacion = formacion;
    }

    public void setEspecializacion(String especializacion) {
        this.especializacion = especializacion;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PerfilEmpleado toPerfilEmpleado() {
        PerfilEmpleado perfilEmpleado = new PerfilEmpleado();
        perfilEmpleado.setId(getId());
        perfilEmpleado.setExperiencia(this.getExperiencia());
        perfilEmpleado.setFormacion(this.getFormacion());
        perfilEmpleado.setEspecializacion(this.getEspecializacion());
        perfilEmpleado.setSector(this.getSector());
        return perfilEmpleado;
    }
}