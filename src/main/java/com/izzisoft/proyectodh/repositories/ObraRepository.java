package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Obra;
import org.springframework.data.repository.CrudRepository;

public interface ObraRepository extends CrudRepository<Obra, Long> {
}
