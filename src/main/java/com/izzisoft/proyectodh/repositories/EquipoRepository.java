package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Equipo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface EquipoRepository extends CrudRepository<Equipo, Long> {
    Optional<List<Equipo>> findByCode(String code);

    @Override
    List<Equipo> findAll();
}
