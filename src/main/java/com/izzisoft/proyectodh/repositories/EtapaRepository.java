package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Etapa;
import org.springframework.data.repository.CrudRepository;

public interface EtapaRepository extends CrudRepository<Etapa, Long> {
}
