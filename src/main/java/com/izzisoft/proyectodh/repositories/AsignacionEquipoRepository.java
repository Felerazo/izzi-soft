package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.AsignacionEquipo;
import org.springframework.data.repository.CrudRepository;

public interface AsignacionEquipoRepository extends CrudRepository<AsignacionEquipo, Long> {
}