package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category, Long> {
    Optional<List<Category>> findByCode(String code);

    @Override
    List<Category> findAll();
}